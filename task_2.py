# У реченні “Hello world” замінити всі літери “o” на “a”, а літери “l” на “e”.

word = "Hello world!"
new_word = ""
for symbol in word:
    if symbol == "o":
        new_word += "a"
    elif symbol == "l":
        new_word += "e"
    else:
        new_word += symbol
print(new_word)

word_list = list(word)
new_word_2 = ""
while word_list:
    symbol = word_list.pop(0)
    if symbol == "o":
        new_word_2 += "a"
    elif symbol == "l":
        new_word_2 += "e"
    else:
        new_word_2 += symbol
print(2, new_word_2)
