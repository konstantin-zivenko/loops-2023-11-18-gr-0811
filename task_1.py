# Вивести у консоль перші 100 чисел Фібоначчі (див. у рекомендованих ресурсах)

for n in range(100):
    if n == 0:
        fib = 0
    elif n == 1:
        fib = 1
        fib_minus_1 = 0
    else:
        fib, fib_minus_1 = fib + fib_minus_1, fib
    print(f"fibonacci({n}) = {fib}")
